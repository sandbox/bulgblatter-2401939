BITBUCKET AUTODEPLOY MODULE FOR DRUPAL 7.x
---------------------------

CONTENTS OF THIS README
-----------------------

    * Description
    * Requirements
    * Installation
    * Support
    * Credits


DESCRIPTION
-----------
Use this module to automate deployment from Bitbucket Repo to your server. The module does not need bitbucket
credentials.
There are basically three things you need to do to make this work:
    * Set up SSH keys so your server can talk to Bitbucket
    * Clone your Bitbucket repository on your web server
    * Setup a hook on Bitbucket and an associated deployment script on your serve

Here’s what your deployment workflow will look like once we’re done:
    * Develop your website locally
    * When you’re ready to deploy, commit your changes and push them to Bitbucket
    * When Bitbucket receives the commit it will notify a deployment script on your server
    * The deployment script will fetch the changes into a cloned repository on your server, and checkout files to
      your public web directory


REQUIREMENTS
------------
    * Git is installed
    * You have shell access
    * The PHP exec function is enabled


INSTALLATION
------------
1. Download, install and enable module as usual
2. Setup SSH keys to connect securely to Bitbucket without a password prompt.
3. Clone Bitbucket repo with --mirror flag in a directory, which can't be accessed via web
        git clone --mirror git@bitbucket.org:<username>/<repo-name>.git
4. Do an initial checkout
        GIT_WORK_TREE=/home/<username>/www git checkout -f <branch-name>
5. Navigate to /admin/config/bitbucket and enter Branch name (same as above), the local Repo dir and www dir
6. If no errors module will provide you with a URL
7. On the Bitbucket website navigate to your repository’s Administration > Hooks screen and add a new POST hook,
pointed at URL, which you can find on /admin/config/bitbucket.
8. Push to Bitbucket will execute the hook and module will do the rest.



CREDITS
-------
Inspired by http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

Follow the lonk above for detailed guide.